﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class CommentDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("rating")]
        public double Rating { get; set; }

        [JsonProperty("pluses")]
        public string Pluses { get; set; }

        [JsonProperty("minuses")]
        public string Minuses { get; set; }

        [JsonProperty("hotelId")]
        public Guid HotelId { get; set; }
    }
}
