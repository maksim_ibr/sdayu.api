﻿using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class UserPasswordDTO
    {
        [JsonProperty("oldPassword")]
        public string OldPassword { get; set; }

        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }
    }
}
