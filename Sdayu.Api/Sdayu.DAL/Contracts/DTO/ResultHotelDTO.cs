﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class ResultHotelDTO : HotelDTO
    {
        [JsonProperty("rating")]
        public double Rating { get; set; }

        [JsonProperty("minPrice")]
        public double MinPrice { get; set; }

        [JsonProperty("azureImages")]
        public List<AzureImageDTO> AzureImages { get; set; } = new List<AzureImageDTO>();
    }
}
