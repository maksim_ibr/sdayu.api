﻿using System;
using Newtonsoft.Json;
using Sdayu.DAL.Types.Domain.Enums;

namespace Sdayu.DAL.Contracts.DTO
{
    public class UserDTO
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("surName")]
        public string SurName { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("birthdayUtc")]
        public DateTime? BirthdayUtc { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("roleToAdd")]
        public Role Role { get; set; }
    }
}
