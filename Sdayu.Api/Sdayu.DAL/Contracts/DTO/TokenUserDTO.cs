﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class FullUserInfoDTO : UserDTO
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("balance")]
        public double Currency { get; set; }

        [JsonProperty("roles")]
        public List<string> Roles = new List<string>();
    }
}
