﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class CardDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("cardNumber")]
        public string CardNumber { get; set; }
    }
}
