﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class RoomDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("hotelId")]
        public Guid HotelId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
        
        [JsonProperty("price")]
        public double Price { get; set; }

        [JsonProperty("peopleCount")]
        public int PeopleCount { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
