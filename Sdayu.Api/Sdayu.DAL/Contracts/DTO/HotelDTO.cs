﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class HotelDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("hotelType")]
        public string HotelType { get; set; }

        [JsonProperty("addressCode")]
        public string AddressCode { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
