﻿using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class UserDataDTO
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
