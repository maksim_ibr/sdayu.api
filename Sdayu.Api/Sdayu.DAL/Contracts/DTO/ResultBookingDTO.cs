﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class ResultBookingDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("room")]
        public Guid RoomId { get; set; }

        [JsonProperty("dateFrom")]
        public DateTime DateFromUtc { get; set; }

        [JsonProperty("dateTo")]
        public DateTime DateToUtc { get; set; }

        [JsonProperty("peopleCount")]
        public int PeopleCount { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }
    }
}
