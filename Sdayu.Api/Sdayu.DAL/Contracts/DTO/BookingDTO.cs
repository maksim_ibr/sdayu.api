﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class BookingDTO
    {
        [JsonProperty("room")]
        public Guid RoomId { get; set; }

        [JsonProperty("dateFrom")]
        public DateTime DateFromUtc { get; set; }

        [JsonProperty("dateTo")]
        public DateTime DateToUtc { get; set; }

        [JsonProperty("adults")]
        public int Adults { get; set; }

        [JsonProperty("children")]
        public int Children { get; set; }
    }
}
