﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class AzureImageDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
