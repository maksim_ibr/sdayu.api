﻿using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class ResultCommentDTO : CommentDTO
    {
        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }
    }
}
