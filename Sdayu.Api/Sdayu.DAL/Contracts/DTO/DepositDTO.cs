﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class DepositDTO
    {
        [JsonProperty("cardId")]
        public Guid CardId { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }
    }
}
