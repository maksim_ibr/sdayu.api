﻿using System;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class FilterDTO
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("roomsCount")]
        public int RoomsCount { get; set; }

        [JsonProperty("adults")]
        public int Adults { get; set; }

        [JsonProperty("children")]
        public int Children { get; set; }

        [JsonProperty("dateFrom")]
        public DateTime DateFromUtc { get; set; }

        [JsonProperty("dateTo")]
        public DateTime DateToUtc { get; set; }
    }
}
