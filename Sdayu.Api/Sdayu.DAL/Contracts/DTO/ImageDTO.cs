﻿using System;

namespace Sdayu.DAL.Contracts.DTO
{
    public class ImageDTO
    {
        private byte[] _image;
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Data { get; set; }

        public byte[] Image => _image ?? (_image = Convert.FromBase64String(Data));
    }

}
