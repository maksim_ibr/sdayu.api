﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class HotelCreateDTO : HotelDTO
    {
        [JsonProperty("images")]
        public List<ImageDTO> Images = new List<ImageDTO>();
    }
}
