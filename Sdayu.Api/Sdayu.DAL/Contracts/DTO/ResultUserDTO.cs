﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class ResultUserDTO : UserDTO
    {
        [JsonIgnore]
        public IdentityResult UserResult { get; set; }

        [JsonIgnore]
        public IdentityResult RoleResult { get; set; }
    }
}
