﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sdayu.DAL.Contracts.DTO
{
    public class ResultRoomDTO : RoomDTO
    {
        [JsonProperty("azureImages")]
        public List<AzureImageDTO> AzureImages { get; set; } = new List<AzureImageDTO>();
    }
}
