﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.DAL.Contracts.Repositories
{
    public interface IRepository<TEntity> where TEntity : class, IEntityBase
    {
        DbSet<TEntity> Set { get; }

        IQueryable<TEntity> GetQueryable(bool asNoTracking = false);

        Task RemoveAsync(TEntity entity, bool commit);

        Task RemoveCollectionAsync(ICollection<TEntity> entities, bool commit);

        Task SaveAsync(TEntity entity, bool commit);

        Task SaveCollectionAsync(ICollection<TEntity> entity, bool commit);

        Task PreCommitSaveAsync();

        Task CommitAsync();
    }

    public interface IRepository<TEntity, in TKey> : IRepository<TEntity> where TEntity : class, IEntityBase<TKey>
    {

    }

}
