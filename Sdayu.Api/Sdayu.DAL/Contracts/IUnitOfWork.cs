﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Sdayu.DAL.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext Context { get; }
        Task CommitAsync();
        void PreCommitSave();
        Task BeginTransactionAsync();
        void Rollback();
        Task SaveChangesAsync();
        Task PreCommitSaveAsync();
    }

}