﻿using System.Threading.Tasks;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.DAL.Contracts.Services
{
    public interface IAccountService : IService
    {
        Task<string> GetUserNameAsync(string userId);

        Task<FullUserInfoDTO> GetByNameAsync(string userName);

        Task<ResultUserDTO> SaveAsync(UserDTO dto, string redirectTo);

        Task<UserDTO> UpdateAsync(UserDTO dto);

        Task<double?> TopUpBalanceAsync(DepositDTO dto);

        Task<double?> WithdrawMoneyAsync(DepositDTO dto);

        Task<bool> ConfirmEmailAsync(string userId, string code);

        Task<bool> IsEmailConfirmedAsync(string userName);

        Task<bool> CheckAndUpdatePasswordAsync(string userId, UserPasswordDTO dto);
    }
}