﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.DAL.Contracts.Services
{
    public interface IHotelService : IService
    {
        Task<bool> DeleteAsync(Guid id);

        Task<ResultHotelDTO> GetAsync(Guid id);

        Task<ICollection<ResultHotelDTO>> GetAllAsync();

        Task<ICollection<ResultHotelDTO>> GetUserHotelsAsync(string userId);

        Task<ResultHotelDTO> CreateAsync(string userId, HotelCreateDTO dto);

        Task<ResultHotelDTO> UpdateAsync(HotelCreateDTO dto);

        Task<ICollection<ResultHotelDTO>> GetFilteredAsync(FilterDTO filter);
    }
}
