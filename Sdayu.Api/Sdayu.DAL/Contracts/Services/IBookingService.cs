﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.DAL.Contracts.Services
{
    public interface IBookingService : IService
    {
        Task<ICollection<ResultBookingDTO>> GeAllBookingsAsync();
    }
}
