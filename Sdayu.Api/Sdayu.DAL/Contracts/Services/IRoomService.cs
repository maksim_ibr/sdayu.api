﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.DAL.Contracts.Services
{
    public interface IRoomService : IService
    {
        Task<bool> DeleteAsync(Guid id);

        Task<ResultRoomDTO> GetAsync(Guid id);

        Task<ICollection<ResultRoomDTO>> GetAllAsync();

        Task<ICollection<ResultRoomDTO>> GetHotelRoomsAsync(Guid hotelId);

        Task<ResultRoomDTO> CreateAsync(RoomCreateDTO dto);

        Task<ResultRoomDTO> UpdateAsync(RoomCreateDTO dto);

        Task<Tuple<double, bool>> BookAsync(string userId, ICollection<BookingDTO> dtoCollection);
    }
}
