﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.DAL.Contracts.Services
{
    public interface ICardService : IService
    {
        Task<bool> DeleteAsync(Guid id);

        Task<CardDTO> GetAsync(Guid id);

        Task<CardDTO> AddCardAsync(CardDTO dto, string userId);

        Task<ICollection<CardDTO>> GetAllUserСardsAsync(string userId);
    }
}