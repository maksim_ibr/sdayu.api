﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.DAL.Contracts.Services
{
    public interface ICommentService : IService
    {
        Task<ICollection<ResultCommentDTO>> GetHotelCommentAsync(Guid hotelId);

        Task<ResultCommentDTO> AddCommentAsync(CommentDTO dto, string userId);
    }
}
