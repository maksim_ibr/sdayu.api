﻿using System.Threading.Tasks;

namespace Sdayu.DAL.Contracts.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
