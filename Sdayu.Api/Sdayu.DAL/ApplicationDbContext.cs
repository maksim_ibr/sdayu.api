﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<Room> Rooms { get; set; }

        public DbSet<Hotel> Hotels { get; set; }

        public DbSet<RoomType> RoomTypes { get; set; }

        public DbSet<HotelType> HotelTypes { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Region> Regions { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Booking> Bookings { get; set; }

        public DbSet<Card> Cards { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Room>().Property(x => x.Title).HasMaxLength(255);
            builder.Entity<RoomType>().Property(x => x.Title).HasMaxLength(255);
            builder.Entity<Hotel>().Property(x => x.Title).HasMaxLength(255);
            builder.Entity<HotelType>().Property(x => x.Title).HasMaxLength(255);
            builder.Entity<City>().Property(x => x.Title).HasMaxLength(255);
            builder.Entity<Region>().Property(x => x.Title).HasMaxLength(255);
            builder.Entity<Country>().Property(x => x.Title).HasMaxLength(255);

            builder.Entity<Country>().HasMany(x => x.Regions).WithOne(x => x.Country).HasForeignKey(x => x.CountryId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Region>().HasMany(x => x.Cities).WithOne(x => x.Region).HasForeignKey(x => x.RegionId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<City>().HasMany(x => x.Hotels).WithOne(x => x.City).HasForeignKey(x => x.CityId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>().HasMany(x => x.Bookings).WithOne(x => x.User).HasForeignKey(x => x.UserId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>().HasMany(x => x.Comments).WithOne(x => x.User).HasForeignKey(x => x.UserId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Room>().HasMany(x => x.Bookings).WithOne(x => x.Room).HasForeignKey(x => x.RoomId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<HotelType>().HasMany(x => x.Hotels).WithOne(x => x.HotelType).HasForeignKey(x => x.HotelTypeId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.SetNull);
            builder.Entity<RoomType>().HasMany(x => x.Rooms).WithOne(x => x.RoomType).HasForeignKey(x => x.RoomTypeId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.SetNull);
            builder.Entity<ApplicationUser>().HasMany(x => x.Cards).WithOne(x => x.User).HasForeignKey(x => x.UserId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Hotel>().HasMany(x => x.HotelImages).WithOne(x => x.Hotel).HasForeignKey(x => x.HotelId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Room>().HasMany(x => x.RoomImages).WithOne(x => x.Room).HasForeignKey(x => x.RoomId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>().HasMany(x => x.Hotels).WithOne(x => x.User).HasForeignKey(x => x.UserId)
                .HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.SetNull);
            base.OnModelCreating(builder);
        }
    }
}