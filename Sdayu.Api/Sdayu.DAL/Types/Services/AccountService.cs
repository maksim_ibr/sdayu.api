﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Sdayu.DAL.Contracts;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Repositories;
using Sdayu.DAL.Contracts.Services;
using Sdayu.DAL.Extensions;
using Sdayu.DAL.Types.Domain;
using Sdayu.DAL.Types.Domain.Enums;

namespace Sdayu.DAL.Types.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IRepository<Card, Guid> _cardRepository;
        private readonly IEmailService _emailService;

        public AccountService(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager,
            IRepository<Card, Guid> cardRepository, IEmailService emailService)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _cardRepository = cardRepository;
            _emailService = emailService;
        }

        public async Task<string> GetUserNameAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
            return user?.Name;
        }

        public async Task<bool> IsEmailConfirmedAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName).ConfigureAwait(false);
            if (user == null)
            {
                return false;
            }

            return await _userManager.IsEmailConfirmedAsync(user).ConfigureAwait(false);
        }

        public async Task<FullUserInfoDTO> GetByNameAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName).ConfigureAwait(false);
            var userDto = Mapper.Map<ApplicationUser, FullUserInfoDTO>(user);
            if (user != null)
            {
                userDto.Roles = (await _userManager.GetRolesAsync(user).ConfigureAwait(false)).ToList();
                userDto.Roles.ForEach(x => x.FirstCharToUpper());
                for (var i = 0; i < userDto.Roles.Count; i++)
                {
                    userDto.Roles[i] = userDto.Roles[i].FirstCharToUpper();
                }
            }

            return userDto;
        }

        public async Task<ResultUserDTO> SaveAsync(UserDTO dto, string redirectTo)
        {
            var user = Mapper.Map<ApplicationUser>(dto);
            var userResult = await _userManager.CreateAsync(user, dto.Password).ConfigureAwait(false);

            IdentityResult roleResult = null;
            if (userResult.Succeeded && dto.Role != 0)
            {
                roleResult = await _userManager.AddToRoleAsync(user, dto.Role.ToString()).ConfigureAwait(false);
            }

            if (userResult.Succeeded)
            {
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
                var codeHtmlVersion = HttpUtility.UrlEncode(code);
                var callbackUrl = $"{redirectTo}?userId={user.Id}&code={codeHtmlVersion}";

                var message = "Hi " + user.UserName + " " +
                              "You have been sent this email because you created an account on our website.  " +
                              "Please click on <a href =\"" + callbackUrl + "\">this link</a> to confirm your email address is correct. ";

                await _emailService.SendEmailAsync(dto.Email, "Confirm your account", message).ConfigureAwait(false);
            }

            var resultUser = new ResultUserDTO
            {
                UserResult = userResult,
                RoleResult = roleResult
            };

            Mapper.Map(user, resultUser);
            return resultUser;
        }

        public async Task<bool> ConfirmEmailAsync(string userId, string code)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return false;
            }

            code = code.Replace('\\', '/');
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return result.Succeeded;

        }

        public async Task<UserDTO> UpdateAsync(UserDTO dto)
        {
            var user = await _userManager.FindByIdAsync(dto.Id).ConfigureAwait(false);
            if (user == null)
            {
                return null;
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                var isInCurrentRole = await _userManager.IsInRoleAsync(user, dto.Role.ToString()).ConfigureAwait(false);
                if (!isInCurrentRole)
                {
                    var roles = ((Role[])Enum.GetValues(typeof(Role))).Select(x => x.ToString());
                    await _userManager.RemoveFromRolesAsync(user, roles).ConfigureAwait(false);
                }

                Mapper.Map(dto, user);
                if (dto.Role != 0)
                {
                    await _userManager.AddToRoleAsync(user, dto.Role.ToString()).ConfigureAwait(false);
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return Mapper.Map<UserDTO>(user);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<bool> CheckAndUpdatePasswordAsync(string userId, UserPasswordDTO dto)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
                var isCorrect = await _userManager.CheckPasswordAsync(user, dto.OldPassword).ConfigureAwait(false);
                if (!isCorrect)
                {
                    return false;
                }

                var token = await _userManager.GeneratePasswordResetTokenAsync(user).ConfigureAwait(false);
                await _userManager.ResetPasswordAsync(user, token, dto.NewPassword).ConfigureAwait(false);
                return true;

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }


        public async Task<double?> TopUpBalanceAsync(DepositDTO dto)
        {
            var card = await _cardRepository
                .GetQueryable()
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == dto.CardId)
                .ConfigureAwait(false);

            if (card == null)
            {
                return null;
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                card.User.Currency += dto.Amount;
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return card.User.Currency;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<double?> WithdrawMoneyAsync(DepositDTO dto)
        {
            var card = await _cardRepository
                .GetQueryable()
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == dto.CardId)
                .ConfigureAwait(false);

            if (card == null)
            {
                return null;
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                if (card.User.Currency <= dto.Amount)
                {
                    card.User.Currency = 0;
                }
                else
                {
                    card.User.Currency -= dto.Amount;
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return card.User.Currency;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }
    }
}
