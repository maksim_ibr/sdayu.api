﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Sdayu.DAL.Contracts;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Repositories;
using Sdayu.DAL.Contracts.Services;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.DAL.Types.Services
{
    public class HotelService : IHotelService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Hotel, Guid> _hotelRepository;
        private readonly IRepository<City, Guid> _cityRepository;
        private readonly IRepository<HotelType, Guid> _hotelTypeRepository;
        private readonly IRepository<HotelImage, Guid> _hotelImageRepository;
        private readonly AzureStorageSettings _azureStorageSettings;

        public HotelService(IUnitOfWork unitOfWork, IRepository<Hotel, Guid> hotelRepository,
            IRepository<City, Guid> cityRepository, IRepository<HotelType, Guid> hotelTypeRepository,
            IRepository<HotelImage, Guid> hotelImageRepository, IOptions<AzureStorageSettings> azureStorageSettings)
        {
            _unitOfWork = unitOfWork;
            _hotelRepository = hotelRepository;
            _cityRepository = cityRepository;
            _hotelTypeRepository = hotelTypeRepository;
            _hotelImageRepository = hotelImageRepository;
            _azureStorageSettings = azureStorageSettings.Value;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var hotel = await _hotelRepository
                .GetQueryable(true)
                .FirstOrDefaultAsync(x => x.Id == id)
                .ConfigureAwait(false);

            if (hotel == null)
            {
                return false;
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                await _hotelRepository.RemoveAsync(hotel, true).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return true;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<ResultHotelDTO> GetAsync(Guid id)
        {
            var hotel = await _hotelRepository
                .GetQueryable(true)
                .Include(x => x.HotelType)
                .Include(x => x.Rooms)
                .Include(x => x.HotelImages)
                .Include(x => x.Comments)
                .FirstOrDefaultAsync(x => x.Id == id)
                .ConfigureAwait(false);

            return Mapper.Map<ResultHotelDTO>(hotel);
        }

        public async Task<ICollection<ResultHotelDTO>> GetUserHotelsAsync(string userId)
        {
            var hotels = await _hotelRepository
                .GetQueryable(true)
                .Include(x => x.HotelImages)
                .Include(x => x.Rooms)
                .Include(x => x.HotelType)
                .Include(x => x.Comments)
                .Where(x => x.UserId == userId)
                .ToListAsync()
                .ConfigureAwait(false);

            return Mapper.Map<List<ResultHotelDTO>>(hotels);
        }

        public async Task<ICollection<ResultHotelDTO>> GetAllAsync()
        {
            var hotels = await _hotelRepository
                .GetQueryable(true)
                .Include(x => x.HotelImages)
                .Include(x => x.Rooms)
                .Include(x => x.HotelType)
                .Include(x => x.Comments)
                .ToListAsync()
                .ConfigureAwait(false);

            return Mapper.Map<List<ResultHotelDTO>>(hotels);
        }

        public async Task<ResultHotelDTO> CreateAsync(string userId, HotelCreateDTO dto)
        {
            var firstCity = await _cityRepository
                .GetQueryable(true)
                .OrderBy(x => x.Id)
                .FirstAsync()
                .ConfigureAwait(false);

            var hotelType = await _hotelTypeRepository
                .GetQueryable(true)
                .FirstOrDefaultAsync(x => x.Title.ToUpper() == dto.HotelType.ToUpper())
                .ConfigureAwait(false);

            var hotel = new Hotel
            {
                CityId = firstCity.Id,
                HotelTypeId = hotelType?.Id,
                UserId = userId
            };
            
            Mapper.Map(dto, hotel);

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                await _hotelRepository.SaveAsync(hotel, true).ConfigureAwait(false);
                var hotelImages = await LoadHotelImagesAsync(dto.Images, hotel.Id).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                var resultHotel = Mapper.Map<ResultHotelDTO>(hotel);
                resultHotel.AzureImages = Mapper.Map<List<AzureImageDTO>>(hotelImages);
                return resultHotel;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<ResultHotelDTO> UpdateAsync(HotelCreateDTO dto)
        {
            var hotelType = await _hotelTypeRepository
                .GetQueryable(true)
                .FirstOrDefaultAsync(x => x.Title.ToUpper() == dto.HotelType.ToUpper())
                .ConfigureAwait(false);

            var hotel = await _hotelRepository
                .GetQueryable()
                .Include(x => x.HotelType)
                .Include(x => x.Rooms)
                .Include(x => x.HotelImages)
                .FirstOrDefaultAsync(x => x.Id == dto.Id)
                .ConfigureAwait(false);

            if (hotel == null)
            {
                return null;
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                Mapper.Map(dto, hotel);
                hotel.HotelTypeId = hotelType?.Id;
                var hotelImages = await LoadHotelImagesAsync(dto.Images, hotel.Id).ConfigureAwait(false);
                var resultHotel = Mapper.Map<ResultHotelDTO>(hotel);
                resultHotel.AzureImages = Mapper.Map<List<AzureImageDTO>>(hotelImages);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return resultHotel;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<ICollection<ResultHotelDTO>> GetFilteredAsync(FilterDTO filter)
        {
            var hotelsInCity = await _hotelRepository
                .GetQueryable(true)
                .Include(x => x.HotelType)
                .Include(x => x.Rooms)
                .ThenInclude(x => x.Bookings)
                .Include(x => x.HotelImages)
                .Where(x => x.Rooms.Any() && x.City.Title == filter.City)
                .ToListAsync()
                .ConfigureAwait(false);

            var filteredHotels = hotelsInCity.Where(x => CheckCondition(filter.RoomsCount,
                filter.Adults + filter.Children,
                x.Rooms.Where(r => !r.Bookings.Any() || r.Bookings.All(a =>
                                       !(a.CheckInUtc >= filter.DateFromUtc && a.CheckOutUtc <= filter.DateToUtc)))
                    .ToList()));

            return Mapper.Map<List<ResultHotelDTO>>(filteredHotels);
        }

        private async Task<ICollection<HotelImage>> LoadHotelImagesAsync(ICollection<ImageDTO> images, Guid hotelId)
        {
            var oldHotelImages = await _hotelImageRepository
                .GetQueryable()
                .Where(x => x.HotelId == hotelId)
                .ToListAsync()
                .ConfigureAwait(false);

            if (oldHotelImages.Any())
            {
                await _hotelImageRepository.RemoveCollectionAsync(oldHotelImages, true).ConfigureAwait(false);
            }

            var hotelImages = new List<HotelImage>();
            if (images != null && images.Any())
            {
                var storageAccount = CloudStorageAccount.Parse(_azureStorageSettings.ConnectionString);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference(_azureStorageSettings.ImagesContainerName);
                await container
                    .CreateIfNotExistsAsync(BlobContainerPublicAccessType.Blob, new BlobRequestOptions(),
                        new OperationContext()).ConfigureAwait(false);

                foreach (var image in images)
                {
                    var imageId = Guid.NewGuid();
                    var blockBlob = container.GetBlockBlobReference($"{hotelId}/{imageId}-{image.FileName}");
                    await blockBlob.UploadFromByteArrayAsync(image.Image, 0, image.Image.Length)
                        .ConfigureAwait(false);

                    var hotelImage = new HotelImage
                    {
                        Id = imageId,
                        HotelId = hotelId,
                        Url = blockBlob.Uri.ToString()
                    };

                    hotelImages.Add(hotelImage);
                    await _hotelImageRepository.Set.AddAsync(hotelImage).ConfigureAwait(false);
                }

                await _hotelImageRepository.PreCommitSaveAsync().ConfigureAwait(false);
            }

            return hotelImages;
        }

        private static bool CheckCondition(int roomCount, int peopleCount, ICollection<Room> freeRooms) => 
            freeRooms.Count >= roomCount && freeRooms.Sum(x => x.PeopleCount) >= peopleCount;
    }
}
