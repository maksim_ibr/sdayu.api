﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Sdayu.DAL.Contracts;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Repositories;
using Sdayu.DAL.Contracts.Services;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.DAL.Types.Services
{
    public class RoomService : IRoomService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Room, Guid> _roomRepository;
        private readonly IRepository<RoomImage, Guid> _roomImageRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AzureStorageSettings _azureStorageSettings;

        public RoomService(IUnitOfWork unitOfWork, IRepository<Room, Guid> roomRepository,
            IRepository<RoomImage, Guid> roomImageRepository, IOptions<AzureStorageSettings> azureStorageSettings,
            UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _roomRepository = roomRepository;
            _roomImageRepository = roomImageRepository;
            _userManager = userManager;
            _azureStorageSettings = azureStorageSettings.Value;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var room = await _roomRepository
                .GetQueryable(true)
                .FirstOrDefaultAsync(x => x.Id == id)
                .ConfigureAwait(false);

            if (room == null)
            {
                return false;
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                await _roomRepository.RemoveAsync(room, true).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return true;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<ResultRoomDTO> GetAsync(Guid id)
        {
            var room = await _roomRepository
                .GetQueryable(true)
                .Include(x => x.RoomImages)
                .FirstOrDefaultAsync(x => x.Id == id)
                .ConfigureAwait(false);

            return Mapper.Map<ResultRoomDTO>(room);
        }

        public async Task<ICollection<ResultRoomDTO>> GetAllAsync()
        {
            var rooms = await _roomRepository
                .GetQueryable(true)
                .ProjectTo<ResultRoomDTO>()
                .ToListAsync()
                .ConfigureAwait(false);

            return rooms;
        }

        public async Task<ICollection<ResultRoomDTO>> GetHotelRoomsAsync(Guid hotelId)
        {
            var rooms = await _roomRepository
                .GetQueryable(true)
                .Where(x => x.HotelId == hotelId)
                .ProjectTo<ResultRoomDTO>()
                .ToListAsync()
                .ConfigureAwait(false);

            return rooms;
        }

        public async Task<ResultRoomDTO> CreateAsync(RoomCreateDTO dto)
        {
            var room = Mapper.Map<Room>(dto);

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                await _roomRepository.SaveAsync(room, true).ConfigureAwait(false);
                var hotelImages = await LoadRoomImagesAsync(dto.Images, room.Id).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                var resultHotel = Mapper.Map<ResultRoomDTO>(room);
                resultHotel.AzureImages = Mapper.Map<List<AzureImageDTO>>(hotelImages);
                return resultHotel;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<ResultRoomDTO> UpdateAsync(RoomCreateDTO dto)
        {
            var room = await _roomRepository
                .GetQueryable()
                .Include(x => x.RoomImages)
                .FirstOrDefaultAsync(x => x.Id == dto.Id)
                .ConfigureAwait(false);

            if (room == null)
            {
                return null;
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                Mapper.Map(dto, room);
                var roomImages = await LoadRoomImagesAsync(dto.Images, room.Id).ConfigureAwait(false);
                var resultRoom = Mapper.Map<ResultRoomDTO>(room);
                resultRoom.AzureImages = Mapper.Map<List<AzureImageDTO>>(roomImages);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return resultRoom;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<Tuple<double, bool>> BookAsync(string userId, ICollection<BookingDTO> dtoCollection)
        {
            var roomIds = dtoCollection.Select(x => x.RoomId);
            var rooms = await _roomRepository
                .GetQueryable()
                .Include(x => x.Bookings)
                .Include(x => x.RoomImages)
                .Where(x => roomIds.Contains(x.Id))
                .ToListAsync()
                .ConfigureAwait(false);

            try
            {
                var any = false;
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);

                var bookedRooms = new List<Room>();
                var user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
                foreach (var room in rooms)
                {
                    var dto = dtoCollection.First(x => x.RoomId == room.Id);
                    if (!room.Bookings.All(x =>
                            dto.DateFromUtc < x.CheckInUtc && dto.DateToUtc < x.CheckInUtc ||
                            dto.DateFromUtc > x.CheckOutUtc && dto.DateToUtc > x.CheckOutUtc) ||
                        room.PeopleCount < dto.Adults + dto.Children)
                    {
                        continue;
                    }

                    any = true;
                    if (user.Currency < room.Price)
                    {
                        throw new Exception("Not enough money");
                    }

                    var booking = new Booking { UserId = userId, Price = room.Price };
                    user.Currency -= room.Price;
                    Mapper.Map(dto, booking);
                    room.Bookings.Add(booking);
                    bookedRooms.Add(room);
                }
                
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return new Tuple<double, bool>(user.Currency, any);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        private async Task<ICollection<RoomImage>> LoadRoomImagesAsync(ICollection<ImageDTO> images, Guid roomId)
        {
            var oldRoomImages = await _roomImageRepository
                .GetQueryable()
                .Where(x => x.RoomId == roomId)
                .ToListAsync()
                .ConfigureAwait(false);

            if (oldRoomImages.Any())
            {
                await _roomImageRepository.RemoveCollectionAsync(oldRoomImages, true).ConfigureAwait(false);
            }

            var roomImages = new List<RoomImage>();
            if (images != null && images.Any())
            {
                var storageAccount = CloudStorageAccount.Parse(_azureStorageSettings.ConnectionString);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference(_azureStorageSettings.ImagesContainerName);
                await container
                    .CreateIfNotExistsAsync(BlobContainerPublicAccessType.Blob, new BlobRequestOptions(),
                        new OperationContext()).ConfigureAwait(false);

                foreach (var image in images)
                {
                    var imageId = Guid.NewGuid();
                    var blockBlob = container.GetBlockBlobReference($"{roomId}/{imageId}-{image.FileName}");
                    await blockBlob.UploadFromByteArrayAsync(image.Image, 0, image.Image.Length)
                        .ConfigureAwait(false);

                    var roomImage = new RoomImage
                    {
                        Id = imageId,
                        RoomId = roomId,
                        Url = blockBlob.Uri.ToString()
                    };

                    roomImages.Add(roomImage);
                    await _roomImageRepository.Set.AddAsync(roomImage).ConfigureAwait(false);
                }

                await _roomImageRepository.PreCommitSaveAsync().ConfigureAwait(false);
            }

            return roomImages;
        }
    }
}
