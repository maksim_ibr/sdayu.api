﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Repositories;
using Sdayu.DAL.Contracts.Services;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.DAL.Types.Services
{
    public class BookingService : IBookingService
    {
        private readonly IRepository<Booking, Guid> _bookingRepository;

        public BookingService(IRepository<Booking, Guid> bookingRepository)
        {
            _bookingRepository = bookingRepository;
        }

        public async Task<ICollection<ResultBookingDTO>> GeAllBookingsAsync()
        {
            var bookings = await _bookingRepository
                .GetQueryable(true)
                .ProjectTo<ResultBookingDTO>()
                .ToListAsync()
                .ConfigureAwait(false);

            return bookings;
        }
    }
}
