﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Sdayu.DAL.Contracts;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Repositories;
using Sdayu.DAL.Contracts.Services;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.DAL.Types.Services
{
    public class CardService : ICardService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Card, Guid> _cardRepository;

        public CardService(IUnitOfWork unitOfWork, IRepository<Card, Guid> cardRepository)
        {
            _unitOfWork = unitOfWork;
            _cardRepository = cardRepository;
        }


        public async Task<bool> DeleteAsync(Guid id)
        {
            var card = await _cardRepository
                .GetQueryable(true)
                .FirstOrDefaultAsync(x => x.Id == id)
                .ConfigureAwait(false);

            if (card == null)
            {
                return false;
            }

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                await _cardRepository.RemoveAsync(card, true).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return true;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }

        public async Task<CardDTO> GetAsync(Guid id)
        {
            var card = await _cardRepository
                .GetQueryable(true)
                .FirstOrDefaultAsync(x => x.Id == id)
                .ConfigureAwait(false);

            return Mapper.Map<CardDTO>(card);
        }

        public async Task<ICollection<CardDTO>> GetAllUserСardsAsync(string userId)
        {
            var cards = await _cardRepository
                .GetQueryable(true)
                .Where(x => x.UserId == userId)
                .ProjectTo<CardDTO>()
                .ToListAsync()
                .ConfigureAwait(false);

            return cards;
        }

        public async Task<CardDTO> AddCardAsync(CardDTO dto, string userId)
        {
            var card = Mapper.Map<Card>(dto);
            card.UserId = userId;

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                await _cardRepository.SaveAsync(card, true).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                return Mapper.Map<CardDTO>(card);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }
    }
}
