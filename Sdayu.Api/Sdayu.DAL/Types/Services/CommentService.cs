﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Sdayu.DAL.Contracts;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Repositories;
using Sdayu.DAL.Contracts.Services;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.DAL.Types.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Comment, Guid> _commentRepository;

        public CommentService(IUnitOfWork unitOfWork, IRepository<Comment, Guid> commentRepository)
        {
            _unitOfWork = unitOfWork;
            _commentRepository = commentRepository;
        }

        public async Task<ICollection<ResultCommentDTO>> GetHotelCommentAsync(Guid hotelId)
        {
            var comments = await _commentRepository
                .GetQueryable(true)
                .Where(x => x.HotelId == hotelId)
                .ProjectTo<ResultCommentDTO>()
                .ToListAsync()
                .ConfigureAwait(false);

            return comments;
        }

        public async Task<ResultCommentDTO> AddCommentAsync(CommentDTO dto, string userId)
        {
            var comment = Mapper.Map<Comment>(dto);
            comment.UserId = userId;

            try
            {
                await _unitOfWork.BeginTransactionAsync().ConfigureAwait(false);
                await _commentRepository.SaveAsync(comment, true).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                return Mapper.Map<ResultCommentDTO>(comment);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex);
                _unitOfWork.Rollback();
                throw;
            }
        }
    }
}
