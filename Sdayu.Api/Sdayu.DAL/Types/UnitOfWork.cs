﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Sdayu.DAL.Contracts;

namespace Sdayu.DAL.Types
{
    public class UnitOfWork<TDbContext> : IUnitOfWork where TDbContext : DbContext
    {
        protected IDbContextTransaction Transaction { get; set; }

        private TDbContext _dbContext;
        public virtual DbContext Context => _dbContext;

        public UnitOfWork(TDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task BeginTransactionAsync()
        {
            if (null == Transaction)
            {
                Transaction = await Context.Database.BeginTransactionAsync().ConfigureAwait(false);
            }
        }

        public virtual void PreCommitSave()
        {
            _dbContext?.SaveChanges();
        }

        public async Task PreCommitSaveAsync()
        {
            if (_dbContext != null)
            {
                await _dbContext.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public virtual async Task CommitAsync()
        {
            if (_dbContext != null)
            {
                await _dbContext.SaveChangesAsync().ConfigureAwait(false);
            }
            if (Transaction != null)
            {
                Transaction.Commit();
                Transaction.Dispose();
                Transaction = null;
            }
        }

        public virtual void Dispose()
        {
            // _dbContext.Dispose();
        }

        public virtual void Rollback()
        {
            if (Transaction != null)
            {
                Transaction.Rollback();
                Transaction.Dispose();
                Transaction = null;
            }
        }

        public virtual async Task SaveChangesAsync()
        {
            if (_dbContext != null)
            {
                await _dbContext.SaveChangesAsync().ConfigureAwait(false);
            }
            if (Transaction != null)
            {
                Transaction.Commit();
                Transaction.Dispose();
                Transaction = null;
            }
        }
    }

}