﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sdayu.DAL.Contracts;
using Sdayu.DAL.Contracts.Repositories;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.DAL.Types.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntityBase
    {
        protected readonly IUnitOfWork UnitOfWork;

        public Repository(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public DbSet<TEntity> Set => UnitOfWork.Context.Set<TEntity>();

        public virtual async Task PreCommitSaveAsync() => await UnitOfWork.PreCommitSaveAsync().ConfigureAwait(false);

        public virtual async Task CommitAsync() => await UnitOfWork.CommitAsync().ConfigureAwait(false);

        public virtual IQueryable<TEntity> GetQueryable(bool asNoTracking = false)
        {
            return asNoTracking ? Set.AsNoTracking() : Set;
        }

        public virtual async Task SaveAsync(TEntity entity, bool commit)
        {
            await Set.AddAsync(entity).ConfigureAwait(false);
            if (commit)
            {
                await UnitOfWork.PreCommitSaveAsync().ConfigureAwait(false);
            }
        }

        public virtual async Task SaveCollectionAsync(ICollection<TEntity> entities, bool commit)
        {
            await Set.AddRangeAsync(entities).ConfigureAwait(false);
            if (commit)
            {
                await UnitOfWork.PreCommitSaveAsync().ConfigureAwait(false);
            }
        }

        public virtual async Task RemoveAsync(TEntity entity, bool commit)
        {
            Set.Remove(entity);
            if (commit)
            {
                await UnitOfWork.PreCommitSaveAsync().ConfigureAwait(false);
            }
        }

        public virtual async Task RemoveCollectionAsync(ICollection<TEntity> entities, bool commit)
        {
            Set.RemoveRange(entities);
            if (commit)
            {
                await UnitOfWork.PreCommitSaveAsync().ConfigureAwait(false);
            }
        }
    }

    public class Repository<TEntity, TKey> : Repository<TEntity>, IRepository<TEntity, TKey> where TEntity : class, IEntityBase<TKey>
    {
        public Repository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override async Task SaveAsync(TEntity entity, bool commit)
        {
            if (Equals(entity.Id, default(TKey)))
            {
                await Set.AddAsync(entity).ConfigureAwait(false);
            }
            else
            {
                Set.Attach(entity);
            }

            if (commit)
            {
                await UnitOfWork.PreCommitSaveAsync().ConfigureAwait(false);
            }
        }

        public override async Task SaveCollectionAsync(ICollection<TEntity> entities, bool commit)
        {
            if (entities.All(x => Equals(x.Id, default(TKey))))
            {
                await Set.AddRangeAsync(entities).ConfigureAwait(false);
            }
            else
            {
                Set.AttachRange(entities);
            }

            if (commit)
            {
                await UnitOfWork.PreCommitSaveAsync().ConfigureAwait(false);
            }
        }
    }

}