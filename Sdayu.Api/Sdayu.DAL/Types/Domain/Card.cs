﻿using System;

namespace Sdayu.DAL.Types.Domain
{
    public class Card : EntityBase<Guid>
    {
        public string CardNumber { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
