﻿using System;
using System.Collections.Generic;

namespace Sdayu.DAL.Types.Domain
{
    public class Hotel : EntityBase<Guid>
    {
        public string Title { get; set; }

        public double Rating { get; set; }

        public string AddressCode { get; set; }

        public string Description { get; set; }

        public Guid? HotelTypeId { get; set; }

        public Guid CityId { get; set; }

        public string UserId { get; set; }

        public virtual City City { get; set; }

        public virtual HotelType HotelType { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }

        public ICollection<HotelImage> HotelImages { get; set; }
    }
}
