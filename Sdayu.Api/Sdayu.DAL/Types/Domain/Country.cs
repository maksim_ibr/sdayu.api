﻿using System;
using System.Collections.Generic;

namespace Sdayu.DAL.Types.Domain
{
    public class Country : EntityBase<Guid>
    {
        public string Title { get; set; }

        public virtual ICollection<Region> Regions { get; set; }
    }
}
