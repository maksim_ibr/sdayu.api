﻿using System;

namespace Sdayu.DAL.Types.Domain
{
    public class HotelImage : EntityBase<Guid>
    {
        public string Url { get; set; }

        public Guid HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }
    }
}
