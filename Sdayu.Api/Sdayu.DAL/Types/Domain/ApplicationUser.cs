﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Sdayu.DAL.Types.Domain
{
    public class ApplicationUser : IdentityUser, IEntityBase<string>
    {
        public string Name { get; set; }

        public string SurName { get; set; }

        public double Currency { get; set; }

        public DateTime? BirthdayUtc { get; set; }

        public virtual ICollection<Hotel> Hotels { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Card> Cards { get; set; }
    }
}
