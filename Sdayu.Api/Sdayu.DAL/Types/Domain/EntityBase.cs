﻿namespace Sdayu.DAL.Types.Domain
{
    public abstract class EntityBase : IEntityBase
    {
    }

    public abstract class EntityBase<T> : EntityBase, IEntityBase<T>
    {
        public virtual T Id { get; set; }
    }
}
