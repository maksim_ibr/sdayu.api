﻿using System;
using System.Collections.Generic;

namespace Sdayu.DAL.Types.Domain
{
    public class Room : EntityBase<Guid>
    {
        public string Title { get; set; }

        public double Price { get; set; }

        public int PeopleCount { get; set; } // ToDo change to adults + children

        public string Description { get; set; }

        public string BedDescription { get; set; }

        public Guid HotelId { get; set; }

        public Guid? RoomTypeId { get; set; }

        public virtual RoomType RoomType { get; set; }

        public virtual Hotel Hotel { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }

        public ICollection<RoomImage> RoomImages { get; set; }
    }
}
