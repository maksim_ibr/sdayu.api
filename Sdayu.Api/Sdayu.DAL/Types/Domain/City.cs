﻿using System;
using System.Collections.Generic;

namespace Sdayu.DAL.Types.Domain
{
    public class City : EntityBase<Guid>
    {
        public string Title { get; set; }

        public Guid? RegionId { get; set; }

        public virtual Region Region { get; set; }

        public virtual ICollection<Hotel> Hotels { get; set; }
    }
}
