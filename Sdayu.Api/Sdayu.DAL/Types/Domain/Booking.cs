﻿using System;

namespace Sdayu.DAL.Types.Domain
{
    public class Booking : EntityBase<Guid>
    {
        public DateTime CheckInUtc { get; set; }

        public DateTime CheckOutUtc { get; set; }

        public int PeopleCount { get; set; }

        public double Price { get; set; }

        public string UserId { get; set; }

        public Guid RoomId { get; set; }

        public virtual Room Room { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
