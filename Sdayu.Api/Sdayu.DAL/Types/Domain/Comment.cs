﻿using System;

namespace Sdayu.DAL.Types.Domain
{
    public class Comment : EntityBase<Guid>
    {
        public double Rating { get; set; }

        public string Pluses { get; set; }

        public string Minuses { get; set; }

        public Guid HotelId { get; set; }

        public string UserId { get; set; }

        public virtual Hotel Hotel { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
