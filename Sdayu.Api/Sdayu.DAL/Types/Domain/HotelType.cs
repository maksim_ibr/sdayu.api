﻿using System;
using System.Collections.Generic;

namespace Sdayu.DAL.Types.Domain
{
    public class HotelType : EntityBase<Guid>
    {
        public string Title { get; set; }

        public virtual ICollection<Hotel> Hotels { get; set; }
    }
}
