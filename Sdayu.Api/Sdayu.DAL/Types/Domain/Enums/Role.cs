﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sdayu.DAL.Types.Domain.Enums
{
    public enum Role
    {
        [EnumMember(Value = "tenant")]
        [Display(Name = "Tenant")]
        Tenant = 1,

        [EnumMember(Value = "landlord")]
        [Display(Name = "Landlord")]
        Landlord = 2,

        [EnumMember(Value = "admin")]
        [Display(Name = "Admin")]
        Admin = 3
    }
}
