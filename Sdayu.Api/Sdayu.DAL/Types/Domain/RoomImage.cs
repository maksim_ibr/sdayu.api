﻿using System;

namespace Sdayu.DAL.Types.Domain
{
    public class RoomImage : EntityBase<Guid>
    {
        public string Url { get; set; }

        public Guid RoomId { get; set; }

        public virtual Room Room { get; set; }
    }
}
