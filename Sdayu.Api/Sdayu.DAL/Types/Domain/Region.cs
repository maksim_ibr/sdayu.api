﻿using System;
using System.Collections.Generic;

namespace Sdayu.DAL.Types.Domain
{
    public class Region : EntityBase<Guid>
    {
        public string Title { get; set; }

        public Guid CountryId { get; set; }

        public virtual Country Country { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }
}
