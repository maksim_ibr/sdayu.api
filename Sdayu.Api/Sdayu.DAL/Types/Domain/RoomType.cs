﻿using System;
using System.Collections.Generic;

namespace Sdayu.DAL.Types.Domain
{
    public class RoomType : EntityBase<Guid>
    {
        public string Title { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }
    }
}
