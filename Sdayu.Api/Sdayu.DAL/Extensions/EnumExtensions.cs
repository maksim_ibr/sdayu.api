﻿using System.Linq;
using System.Runtime.Serialization;

namespace Sdayu.DAL.Extensions
{
    public static class EnumExtensions
    {
        public static string GetEnumMemberAttrValue<T>(this T enumVal)
        {
            var enumType = typeof(T);
            var memInfo = enumType.GetMember(enumVal.ToString());
            var attr = memInfo.FirstOrDefault()?.GetCustomAttributes(false).OfType<EnumMemberAttribute>()
                .FirstOrDefault();
            return attr?.Value;
        }
    }
}
