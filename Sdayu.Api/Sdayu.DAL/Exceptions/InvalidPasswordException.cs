﻿using System;

namespace Sdayu.DAL.Exceptions
{
    public class InvalidPasswordException : Exception
    {
        public override string Message { get; }

        public InvalidPasswordException(string message) :
            base(message)
        {
            Message = message;
        }
    }
}
