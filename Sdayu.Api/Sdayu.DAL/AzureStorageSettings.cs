﻿namespace Sdayu.DAL
{
    public class AzureStorageSettings
    {
        public string ConnectionString { get; set; }
        public string ImagesContainerName { get; set; }
    }
}
