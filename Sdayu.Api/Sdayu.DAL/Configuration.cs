﻿namespace Sdayu.DAL
{
    public class Configuration
    {
        public string Authority { get; set; }

        public string AllowedCloudOrigin { get; set; }
    }
}
