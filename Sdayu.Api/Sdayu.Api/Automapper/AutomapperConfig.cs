﻿using System.Linq;
using AutoMapper;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Types.Domain;

namespace Sdayu.Api.Automapper
{
    public class AutomapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<UserDTO, ApplicationUser>()
                    .ForMember(x => x.Id, opt => opt.Ignore());
                cfg.CreateMap<ApplicationUser, UserDTO>();

                cfg.CreateMap<Room, ResultRoomDTO>()
                    .ForMember(x => x.AzureImages, x => x.MapFrom(y => y.RoomImages));

                cfg.CreateMap<RoomCreateDTO, Room>()
                    .ForMember(x => x.Id, opt => opt.Ignore());

                cfg.CreateMap<Hotel, ResultHotelDTO>()
                    .ForMember(x => x.MinPrice, x => x.MapFrom(y => y.Rooms.Any() ? y.Rooms.Min(m => m.Price) : 0))
                    .ForMember(x => x.Rating,
                        x => x.MapFrom(y =>
                            y.Comments != null && y.Comments.Any() ? y.Comments.Select(s => s.Rating).Average() : 0))
                    .ForMember(x => x.AzureImages, x => x.MapFrom(y => y.HotelImages))
                    .ForMember(x => x.HotelType, x => x.MapFrom(y => y.HotelType));

                cfg.CreateMap<HotelCreateDTO, Hotel>()
                    .ForMember(x => x.Id, opt => opt.Ignore())
                    .ForMember(x => x.CityId, opt => opt.Ignore())
                    .ForMember(x => x.HotelTypeId, opt => opt.Ignore())
                    .ForMember(x => x.HotelType, opt => opt.Ignore())
                    .ForMember(x => x.UserId, opt => opt.Ignore());

                cfg.CreateMap<HotelImage, AzureImageDTO>();

                cfg.CreateMap<RoomImage, AzureImageDTO>();

                cfg.CreateMap<CommentDTO, Comment>()
                    .ForMember(x => x.Id, opt => opt.Ignore());
                cfg.CreateMap<Comment, ResultCommentDTO>();

                cfg.CreateMap<CardDTO, Card>()
                    .ForMember(x => x.Id, opt => opt.Ignore());

                cfg.CreateMap<Card, CardDTO>()
                    .ForMember(x => x.CardNumber,
                        x => x.MapFrom(y =>
                            $"{y.CardNumber.Substring(0, 4)}-****-****-{y.CardNumber.Substring(y.CardNumber.Length - 4)}"));

                cfg.CreateMap<BookingDTO, Booking>() // ToDo convert to utc time
                    .ForMember(x => x.CheckInUtc, x => x.MapFrom(y => y.DateFromUtc))
                    .ForMember(x => x.CheckOutUtc, x => x.MapFrom(y => y.DateToUtc))
                    .ForMember(x => x.PeopleCount, x => x.MapFrom(y => y.Adults + y.Children)) // todo
                    .ForMember(x => x.UserId, opt => opt.Ignore())
                    .ForMember(x => x.Price, opt => opt.Ignore());

                cfg.CreateMap<Booking, ResultBookingDTO>()
                    .ForMember(x => x.DateFromUtc, x => x.MapFrom(y => y.CheckInUtc))
                    .ForMember(x => x.DateToUtc, x => x.MapFrom(y => y.CheckOutUtc));

                cfg.CreateMap<ResultUserDTO, UserDTO>();
                cfg.CreateMap<ApplicationUser, ResultUserDTO>()
                    .ForMember(x => x.UserResult, opt => opt.Ignore())
                    .ForMember(x => x.RoleResult, opt => opt.Ignore());

                cfg.CreateMap<ApplicationUser, FullUserInfoDTO>()
                    .ForMember(x => x.Token, opt => opt.Ignore())
                    .ForMember(x => x.Roles, opt => opt.Ignore());
            });
        }
    }
}
