﻿using System.Security.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Sdayu.Api.Controllers
{
    [Authorize]
    public class AuthenticatedController : ControllerBase
    {
        protected readonly IHttpContextAccessor HttpContextAccessor;

        public string UserId => HttpContextAccessor.HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value ??
                                   throw new AuthenticationException("User is not authorized!");

        public AuthenticatedController(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }
    }
}