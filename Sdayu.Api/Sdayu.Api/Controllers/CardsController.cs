﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Services;

namespace Sdayu.Api.Controllers
{
    [Authorize]
    [Route("api/cards")]
    public class CardsController : AuthenticatedController
    {
        private readonly ICardService _cardService;

        public CardsController(IHttpContextAccessor httpContextAccessor, ICardService cardService) :
            base(httpContextAccessor)
        {
            _cardService = cardService;
        }

        [HttpGet("{id}"),
         SwaggerResponse(HttpStatusCode.OK, typeof(CardDTO))]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var result = await _cardService.GetAsync(id).ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet(""),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<CardDTO>))]
        public async Task<IActionResult> GetAllUserСardsAsync()
        {
            var result = await _cardService.GetAllUserСardsAsync(UserId).ConfigureAwait(true);
            return Ok(result);
        }

        [HttpPost("add"),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<CardDTO>))]
        public async Task<IActionResult> AddCardAsync([FromBody] CardDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _cardService.AddCardAsync(dto, UserId).ConfigureAwait(true);
            return Ok(result);
        }

        [HttpDelete("{id}"),
         SwaggerResponse(HttpStatusCode.OK, null)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var result = await _cardService.DeleteAsync(id).ConfigureAwait(true);
            if (!result)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}