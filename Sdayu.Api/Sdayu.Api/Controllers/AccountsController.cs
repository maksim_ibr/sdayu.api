﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using NSwag.Annotations;
using Sdayu.DAL;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Services;
using Sdayu.DAL.Types.Domain.Enums;

namespace Sdayu.Api.Controllers
{
    [Authorize]
    [Route("api/accounts")]
    public class AccountsController : AuthenticatedController
    {
        private readonly IAccountService _accountService;
        private readonly Configuration _configuration;

        public AccountsController(IAccountService accountService, IOptionsMonitor<Configuration> configuration,
            IHttpContextAccessor httpContextAccessor) :
            base(httpContextAccessor)
        {
            _accountService = accountService;
            _configuration = configuration.CurrentValue;
        }

        [HttpPost("withdraw-money"),
         SwaggerResponse(HttpStatusCode.OK, typeof(double?))]
        public async Task<IActionResult> WithdrawMoneyAsync([FromBody] DepositDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _accountService.WithdrawMoneyAsync(dto).ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpPost("top-up-balance"),
         SwaggerResponse(HttpStatusCode.OK, typeof(double?))]
        public async Task<IActionResult> TopUpBalanceAsync([FromBody] DepositDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _accountService.TopUpBalanceAsync(dto).ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("register"),
         SwaggerResponse(HttpStatusCode.Created, typeof(UserDTO))]
        public async Task<IActionResult> RegisterUserAsync([FromBody] UserDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrEmpty(dto.Password))
            {
                return BadRequest(new InvalidDataError("Password can not be empty"));
            }

            var result = await _accountService.SaveAsync(dto, $"{_configuration.AllowedCloudOrigin}/api/accounts/confirm-email")
                .ConfigureAwait(true);
            if (result.UserResult.Succeeded && result.RoleResult.Succeeded)
            {
                return Created("api/accounts/register", Mapper.Map<ResultUserDTO, UserDTO>(result));
            }

            var allErrors =
                (result.UserResult.Errors ?? Enumerable.Empty<IdentityError>()).Concat(
                    result.RoleResult?.Errors ?? Enumerable.Empty<IdentityError>()).ToList();

            return BadRequest(new InvalidDataError(allErrors[0].Description));
        }

        [AllowAnonymous]
        [HttpGet("confirm-email"),
         SwaggerResponse(HttpStatusCode.OK, null)]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return BadRequest("Empty data");
            }

            var result = await _accountService.ConfirmEmailAsync(userId, code).ConfigureAwait(true);

            if (result)
            {
                return Content("Email confirmed");
            }

            return BadRequest(new InvalidDataError("Email not confirmed"));
        }

        [HttpPut("update"),
         SwaggerResponse(HttpStatusCode.Created, typeof(UserDTO))]
        public async Task<IActionResult> UpdateUserAsync([FromBody] UserDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _accountService.UpdateAsync(dto).ConfigureAwait(true);
            if (result != null)
            {
                return Ok(result);
            }
            
            return BadRequest();
        }

        [HttpPut("update-password"),
         SwaggerResponse(HttpStatusCode.OK, null)]
        public async Task<IActionResult> UpdatePasswordAsync([FromBody] UserPasswordDTO dto)
        {
            var result = await _accountService.CheckAndUpdatePasswordAsync(UserId, dto).ConfigureAwait(true);
            if (result)
            {
                return Ok();
            }
            return BadRequest(new InvalidDataError("Invalid password"));

        }

        [AllowAnonymous]
        [HttpPost("token"),
         SwaggerResponse(HttpStatusCode.OK, typeof(FullUserInfoDTO))]
        public async Task<IActionResult> GetTokenAsync([FromBody] UserDataDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var isEmailConfirmed = await _accountService.IsEmailConfirmedAsync(dto.UserName).ConfigureAwait(true);
            if (!isEmailConfirmed)
            {
                return BadRequest(new InvalidDataError("UserName not found or email not confirmed"));
            }

            var content = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                { "grant_type", "password" },
                { "client_id", AuthConfiguration.ClientId },
                { "client_secret", AuthConfiguration.ClientSecret },
                { "username", dto.UserName },
                { "password", dto.Password },
                { "scope", AuthConfiguration.Scope }
            });

            var client = new HttpClient();
            var tokenResponse = await client.PostAsync($"{_configuration.AllowedCloudOrigin}/connect/token", content)
                .ConfigureAwait(false);

            if (!tokenResponse.IsSuccessStatusCode)
            {
                return Unauthorized();
            }

            var token =
                JObject.Parse(await tokenResponse.Content.ReadAsStringAsync().ConfigureAwait(false))["access_token"]
                    .ToString();

            var user = await _accountService.GetByNameAsync(dto.UserName).ConfigureAwait(true);
            if (user.Roles.Any())
            {
                user.Role = Enum.Parse<Role>(user.Roles[0]);
            }

            user.Token = token;

            return Ok(user);
        }
    }
}