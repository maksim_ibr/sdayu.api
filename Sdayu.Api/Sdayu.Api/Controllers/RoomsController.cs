﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Services;

namespace Sdayu.Api.Controllers
{
    [Authorize]
    [Route("api/rooms")]
    public class RoomsController : AuthenticatedController
    {
        private readonly IRoomService _roomService;
        private readonly IBookingService _bookingService;

        public RoomsController(IHttpContextAccessor httpContextAccessor, IRoomService roomsService,
            IBookingService bookingService) :
            base(httpContextAccessor)
        {
            _roomService = roomsService;
            _bookingService = bookingService;
        }

        [HttpGet("{id}"),
         SwaggerResponse(HttpStatusCode.OK, typeof(ResultRoomDTO))]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var result = await _roomService.GetAsync(id).ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet(""),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<ResultRoomDTO>))]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _roomService.GetAllAsync().ConfigureAwait(true);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("hotels/{hotelId}"),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<ResultRoomDTO>))]
        public async Task<IActionResult> GetHotelRoomsAsync(Guid hotelId)
        {
            var result = await _roomService.GetHotelRoomsAsync(hotelId).ConfigureAwait(true);
            return Ok(result);
        }

        [HttpGet("bookings"),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<BookingDTO>))]
        public async Task<IActionResult> GeAllBookingsAsync()
        {
            var result = await _bookingService.GeAllBookingsAsync().ConfigureAwait(true);
            return Ok(result);
        }

        [HttpPost("create"),
         SwaggerResponse(HttpStatusCode.Created, typeof(ICollection<ResultRoomDTO>))]
        public async Task<IActionResult> CreateAsync([FromBody] RoomCreateDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _roomService.CreateAsync(dto).ConfigureAwait(true);
            return Created("api/rooms/create", result);
        }

        [HttpPut("update"),
         SwaggerResponse(HttpStatusCode.Created, typeof(ICollection<ResultRoomDTO>))]
        public async Task<IActionResult> UpdateAsync([FromBody] RoomCreateDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _roomService.UpdateAsync(dto).ConfigureAwait(true);
            return Ok(result);
        }

        [HttpPost("booking"),
         SwaggerResponse(HttpStatusCode.Created, typeof(string))]
        public async Task<IActionResult> BookAsync([FromBody] ICollection<BookingDTO> dtoCollection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var result = await _roomService.BookAsync(UserId, dtoCollection).ConfigureAwait(true);
                if (!result.Item2)
                {
                    return NotFound();
                }

                return Created("api/rooms/booking", result.Item1);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest(new InvalidDataError(e.Message));
            }
        }

        [HttpDelete("{id}"),
         SwaggerResponse(HttpStatusCode.OK, null)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var result = await _roomService.DeleteAsync(id).ConfigureAwait(true);
            if (!result)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}