﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Services;

namespace Sdayu.Api.Controllers
{
    [Authorize]
    [Route("api/hotels")]
    public class HotelsController : AuthenticatedController
    {
        private readonly IAccountService _accountService;
        private readonly IHotelService _hotelService;
        private readonly ICommentService _commentService;

        public HotelsController(IHttpContextAccessor httpContextAccessor, IHotelService hotelsService,
            ICommentService commentService, IAccountService accountService) :
            base(httpContextAccessor)
        {
            _hotelService = hotelsService;
            _commentService = commentService;
            _accountService = accountService;
        }

        [AllowAnonymous]
        [HttpGet("{id}"),
         SwaggerResponse(HttpStatusCode.OK, typeof(HotelDTO))]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var result = await _hotelService.GetAsync(id).ConfigureAwait(true);
            if (result == null)
            {
                return NotFound();
            }
       
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet(""),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<HotelDTO>))]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _hotelService.GetAllAsync().ConfigureAwait(true);
            return Ok(result);
        }

        [HttpGet("own"),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<HotelDTO>))]
        public async Task<IActionResult> GetUserHotelsAsync()
        {
            var result = await _hotelService.GetUserHotelsAsync(UserId).ConfigureAwait(true);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("filter"),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<HotelDTO>))]
        public async Task<IActionResult> GetFilteredAsync([FromBody] FilterDTO filter)
        {
            var result = await _hotelService.GetFilteredAsync(filter).ConfigureAwait(true);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("comments/{hotelId}"),
         SwaggerResponse(HttpStatusCode.OK, typeof(ICollection<ResultCommentDTO>))]
        public async Task<IActionResult> GetHotelCommentAsync(Guid hotelId)
        {
            var result = await _commentService.GetHotelCommentAsync(hotelId).ConfigureAwait(true);
            return Ok(result);
        }

        [HttpPost("comments/add"),
         SwaggerResponse(HttpStatusCode.Created, typeof(ResultCommentDTO))]
        public async Task<IActionResult> AddCommentAsync([FromBody] CommentDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _commentService.AddCommentAsync(dto, UserId).ConfigureAwait(true);
            result.UserName = await _accountService.GetUserNameAsync(UserId).ConfigureAwait(false);
            return Created($"api/hotels/{dto.HotelId}/comments/add", result);
        }

        [HttpPost("create"),
         SwaggerResponse(HttpStatusCode.Created, typeof(ICollection<ResultHotelDTO>))]
        public async Task<IActionResult> CreateAsync([FromBody] HotelCreateDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _hotelService.CreateAsync(UserId, dto).ConfigureAwait(true);
            return Created("api/hotels/create", result);
        }

        [HttpPut("update"),
         SwaggerResponse(HttpStatusCode.Created, typeof(ICollection<ResultHotelDTO>))]
        public async Task<IActionResult> UpdateAsync([FromBody] HotelCreateDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _hotelService.UpdateAsync(dto).ConfigureAwait(true);
            return Ok(result);
        }

        [HttpDelete("{id}"),
         SwaggerResponse(HttpStatusCode.OK, null)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var result = await _hotelService.DeleteAsync(id).ConfigureAwait(true);
            if (!result)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}