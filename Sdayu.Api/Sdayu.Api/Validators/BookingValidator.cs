﻿using System;
using FluentValidation;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.Api.Validators
{
    public class BookingValidator : AbstractValidator<BookingDTO>
    {
        public BookingValidator()
        {
            RuleFor(x => x.RoomId).NotEmpty().WithMessage("RoomId can not be empty");
            RuleFor(x => x.DateFromUtc).GreaterThanOrEqualTo(DateTime.UtcNow).WithMessage("DateFrom is not valid");
            RuleFor(x => x.DateToUtc).GreaterThanOrEqualTo(DateTime.UtcNow).WithMessage("DateTo is not valid");
            RuleFor(x => x.DateToUtc).GreaterThan(x => x.DateFromUtc).WithMessage("DateTo is not valid");
        }
    }
}
