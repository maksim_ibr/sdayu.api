﻿using FluentValidation;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.Api.Validators
{
    public class CommentValidator : AbstractValidator<CommentDTO>
    {
        public CommentValidator()
        {
            RuleFor(x => x.HotelId).NotEmpty().WithMessage("HotelId can not be empty");
            RuleFor(x => x.Rating).NotEmpty().GreaterThanOrEqualTo(0).LessThanOrEqualTo(10)
                .WithMessage("The Rating must be from 0 to 10");
        }
    }
}
