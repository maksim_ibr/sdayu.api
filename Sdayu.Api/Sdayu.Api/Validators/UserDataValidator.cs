﻿using FluentValidation;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.Api.Validators
{
    public class UserDataValidator : AbstractValidator<UserDataDTO>
    {
        public UserDataValidator()
        {
            RuleFor(x => x.UserName).NotEmpty().WithMessage("UserName can not be empty");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Password can not be empty");
        }
    }
}