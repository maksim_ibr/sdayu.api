﻿using FluentValidation;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.Api.Validators
{
    public class HotelValidator : AbstractValidator<HotelCreateDTO>
    {
        public HotelValidator()
        {
            RuleFor(x => x.AddressCode).NotEmpty().WithMessage("Address can not be empty");
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title can not be empty");
        }
    }
}
