﻿using FluentValidation;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.Api.Validators
{
    public class CardValidator : AbstractValidator<CardDTO>
    {
        public CardValidator()
        {
            RuleFor(x => x.CardNumber).NotEmpty().WithMessage("card number can not be empty");
        }
    }
}
