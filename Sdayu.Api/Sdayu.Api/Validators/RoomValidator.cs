﻿using FluentValidation;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.Api.Validators
{
    public class RoomValidator : AbstractValidator<RoomCreateDTO>
    {
        public RoomValidator()
        {
            RuleFor(x => x.HotelId).NotEmpty().WithMessage("HotelId can not be empty");
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title can not be empty");
            RuleFor(x => x.PeopleCount).NotEmpty().WithMessage("Enter the number of people");
            RuleFor(x => x.Price).NotEmpty().WithMessage("Specify the price");
        }
    }
}
