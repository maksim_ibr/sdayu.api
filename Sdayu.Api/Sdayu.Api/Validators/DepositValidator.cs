﻿using FluentValidation;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.Api.Validators
{
    public class DepositValidator : AbstractValidator<DepositDTO>
    {
        public DepositValidator()
        {
            RuleFor(x => x.CardId).NotEmpty().WithMessage("CardId can not be empty");
            RuleFor(x => x.Amount).NotNull().WithMessage("Amount is not valid");
        }
    }
}
