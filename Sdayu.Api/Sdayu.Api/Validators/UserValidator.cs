﻿using FluentValidation;
using Sdayu.DAL.Contracts.DTO;

namespace Sdayu.Api.Validators
{
    public class UserValidator : AbstractValidator<UserDTO>
    {
        public UserValidator()
        {
            RuleFor(x => x.UserName).NotEmpty().WithMessage("UserName can not be empty");
        }
    }
}
