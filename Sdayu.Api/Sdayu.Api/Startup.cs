﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NJsonSchema;
using NSwag;
using NSwag.AspNetCore;
using NSwag.SwaggerGeneration.Processors.Security;
using Sdayu.Api.Automapper;
using Sdayu.Api.Validators;
using Sdayu.DAL;
using Sdayu.DAL.Contracts;
using Sdayu.DAL.Contracts.DTO;
using Sdayu.DAL.Contracts.Repositories;
using Sdayu.DAL.Contracts.Services;
using Sdayu.DAL.Extensions;
using Sdayu.DAL.Types;
using Sdayu.DAL.Types.Domain;
using Sdayu.DAL.Types.Domain.Enums;
using Sdayu.DAL.Types.Repositories;
using Sdayu.DAL.Types.Services;
using FluentValidation.AspNetCore;
using Sdayu.Api.Services;


namespace Sdayu.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            AutomapperConfig.Initialize();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AzureStorageSettings>(Configuration.GetSection("Data:Azure"));
            var authSettingsOptions = Configuration.GetSection("AuthSettings");
            //var google = Configuration.GetSection("google");

            var allowedCloudOrigin = authSettingsOptions.GetValue<string>("AllowedCloudOrigin");

            services.AddCors(options =>
            {
                options.AddPolicy("DefaultPolicy", policy =>
                {
                    policy
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });
            });

            var connectionString = GetConnectionString(Configuration);
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
                {
                    options.Password.RequiredLength = 4;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireDigit = false;

                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryPersistedGrants()
                .AddInMemoryIdentityResources(AuthConfiguration.GetIdentityResources())
                .AddInMemoryApiResources(AuthConfiguration.GetApiResources())
                .AddInMemoryClients(AuthConfiguration.GetClients())
                .AddAspNetIdentity<ApplicationUser>();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.Authority = allowedCloudOrigin;
                    options.Audience = AuthConfiguration.Scope;
                    options.RequireHttpsMetadata = false;
                    //options.UseGoogle( // bug
                    //    clientId: google.GetValue<string>("client_id"),
                    //    hostedDomain: authority);
                });

            services
                .AddMvc()
                .AddFluentValidation();

            services.AddSwagger();
            services.AddOptions();

            services.AddTransient<IUnitOfWork, UnitOfWork<ApplicationDbContext>>();
            services.AddTransient(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));

            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IRoomService, RoomService>();
            services.AddTransient<IHotelService, HotelService>();
            services.AddTransient<ICardService, CardService>();
            services.AddTransient<IBookingService, BookingService>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<IEmailService, EmailService>();

            services.AddTransient<IValidator<UserDTO>, UserValidator>();
            services.AddTransient<IValidator<UserDataDTO>, UserDataValidator>();
            services.AddTransient<IValidator<DepositDTO>, DepositValidator>();
            services.AddTransient<IValidator<BookingDTO>, BookingValidator>();
            services.AddTransient<IValidator<CardDTO>, CardValidator>();
            services.AddTransient<IValidator<RoomCreateDTO>, RoomValidator>();
            services.AddTransient<IValidator<HotelCreateDTO>, HotelValidator>();
            services.AddTransient<IValidator<CommentDTO>, CommentValidator>();

            services.Configure<Configuration>(authSettingsOptions);
        }

        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCors("DefaultPolicy");
            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseMvcWithDefaultRoute();

            app.UseSwaggerUi3WithApiExplorer(settings =>
            {
                settings.GeneratorSettings.DefaultPropertyNameHandling =
                    PropertyNameHandling.CamelCase;
                settings.DocExpansion = "list";
                settings.GeneratorSettings.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT token"));
                settings.GeneratorSettings.DocumentProcessors.Add(new SecurityDefinitionAppender("JWT token",
                    new SwaggerSecurityScheme
                    {
                        Type = SwaggerSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        Description = "Copy 'Bearer ' + valid JWT token into field",
                        In = SwaggerSecurityApiKeyLocation.Header,
                    }));
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            await InitializeDatabaseAsync(app).ConfigureAwait(false);
        }

        private static async Task InitializeDatabaseAsync(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                context.Database.Migrate();

                const string adminEmail = "admin@gg.com";

                var rolesSeeded = true;
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                foreach (var role in (Role[])Enum.GetValues(typeof(Role)))
                {
                    var roleName = role.GetEnumMemberAttrValue();
                    var roleSeeded =
                        await context.Roles.AnyAsync(x => x.NormalizedName == roleName.ToUpper());
                    if (!roleSeeded)
                    {
                        rolesSeeded = false;
                        await roleManager.CreateAsync(new IdentityRole(roleName)).ConfigureAwait(true);
                    }
                }

                var citiesSeeded = await context.Cities.AnyAsync().ConfigureAwait(true);
                if (!citiesSeeded)
                {
                    var cityManager = scope.ServiceProvider.GetRequiredService<IRepository<City, Guid>>();
                    var city = new City { Title = "Minsk" };
                    await cityManager.SaveAsync(city, true).ConfigureAwait(false);
                }

                var usersSeeded = await context.Users.AnyAsync().ConfigureAwait(true);
                if (!usersSeeded)
                {
                    var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                    var admin = new ApplicationUser
                    {
                        Email = adminEmail,
                        UserName = adminEmail
                    };

                    await userManager.CreateAsync(admin, "dae0vi8O").ConfigureAwait(true);
                    await userManager.AddToRoleAsync(admin, Role.Admin.GetEnumMemberAttrValue()).ConfigureAwait(true);
                }

                if (!rolesSeeded || !usersSeeded || !citiesSeeded)
                {
                    await context.SaveChangesAsync().ConfigureAwait(true);
                }
            }
        }

        public static string GetConnectionString(IConfiguration configuration) =>
            configuration.GetConnectionString("DefaultConnection");
    }
}
