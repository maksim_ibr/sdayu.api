﻿using System.Net;

namespace Sdayu.Api
{
    public class InvalidDataError : ApiError
    {
        public InvalidDataError()
            : base(400, HttpStatusCode.InternalServerError.ToString())
        {
        }


        public InvalidDataError(string message)
            : base(400, HttpStatusCode.InternalServerError.ToString(), message)
        {
        }
    }
}
